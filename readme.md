Each solution can be run with python3.6+, no additional libraries required.
Most of the code should be completely self-explanatory.
To run with docker from the current directory, run the following from bash:
```
docker run -v $(realpath .):/project-euler python:3.6 python /project-euler/problem<problem_number>.py
```
e.g.
```
docker run -v $(realpath .):/project-euler python:3.6 python /project-euler/problem31.py
```

Solution for problem 31 is written using a recursive approach but the performance can be made optimal
by adding memoization to the `ways_to_deal_with` call which is trivial.

Links to and descriptions of the problems can be found in each file.
