# https://projecteuler.net/problem=31
# In the United Kingdom the currency is made up of pound (£) and pence (p). There are eight coins in general circulation:
#
# 1p, 2p, 5p, 10p, 20p, 50p, £1 (100p), and £2 (200p).
# It is possible to make £2 in the following way:
#
# 1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
# How many different ways can £2 be made using any number of coins?

COINS = [1, 2, 5, 10, 20, 50, 100, 200]


# TODO: This can be memoized to make the performance optimal, if necessary
# this will improve runtime and reduce maximum call stack size
def ways_to_deal_with(amount, allowed_coins):
    ways = 0

    for i, coin in enumerate(allowed_coins):
        remainder = amount - coin
        if remainder == 0:
            ways += 1
        elif remainder > 0:
            ways += ways_to_deal_with(remainder, allowed_coins[:i + 1])

    return ways


def ways_to_deal(amount):
    if amount < 1:
        # Assuming this is the expect return in this case
        return 0

    return ways_to_deal_with(amount, COINS)


def expect_counts_for_amount(amount, count):
    actual = ways_to_deal(amount)
    if actual != count:
        raise RuntimeError(f"For amount {amount} expected {count} but got {actual}")
    else:
        print(f"Succeeded for amount={amount}")


if __name__ == "__main__":
    expect_counts_for_amount(0, 0)
    expect_counts_for_amount(1, 1)   # 1
    expect_counts_for_amount(2, 2)   # 11,      2
    expect_counts_for_amount(3, 2)   # 111,    12
    expect_counts_for_amount(4, 3)   # 111,    112,   22
    expect_counts_for_amount(5, 4)   # 11111,  1112,  122,  5
    expect_counts_for_amount(6, 5)   # 111111, 11112, 1122, 15, 222
    expect_counts_for_amount(7, 6)   # 1111111, 111112, 11122, 1222, 115, 25
