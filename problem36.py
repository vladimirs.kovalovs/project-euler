# https://projecteuler.net/problem=36
# The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.
#
# Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
#
# (Please note that the palindromic number, in either base, may not include leading zeros.)


def int_to_list_with_base(i, base):
    digits = []
    while i > 0:
        digits.append(i % base)
        i = i // base
    return digits


def palindrome_in_base(x, base):
    return int_to_list_with_base(x, base) == list(reversed(int_to_list_with_base(x, base)))


def palindrome_in_2_and_10(x):
    return palindrome_in_base(x, 10) and palindrome_in_base(x, 2)


if __name__ == "__main__":
    matching_numbers = []
    for x in range(1000000):
        if palindrome_in_2_and_10(x):
            matching_numbers.append(x)

    print(f"Answer={sum(matching_numbers)}")
